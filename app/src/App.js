import React, { Component } from "react";
import axios from 'axios';
import Table from './Components/Table'
import Form from './Components/Form';

class App extends Component {
  state = {
      questions: [],
      intervalIsSet: true
  };

  handleSubmit = question => {
    this.setState({ questions: [...this.state.questions, question] });
    console.log(question);
    this.postDataToDB(question);
  };

  getQuestion = (index) => {    
      this.getDataFromDb(null, index);
  };

  removeQuestion = index => {
    const { questions } = this.state

    this.deleteFromDB(index);

    this.setState({
      questions: questions.filter((question, i) => {
        return i !== index
      }),
    })
  };

  componentDidMount() {
      let search = window.location.search;
      let params = new URLSearchParams(search);    

      this.getDataFromDb(params.get('page'), params.get('id'));

      if (!this.state.intervalIsSet) {
        let interval = setInterval(this.getDataFromDb, 1000);

        this.setState({ intervalIsSet: interval });
      }
  };

  componentWillUnmount() {
    if (this.state.intervalIsSet) {
        clearInterval(this.state.intervalIsSet);
        this.setState({ intervalIsSet: false });
    }
  };

  getDataFromDb = (page, id) => {
    console.log(page, id);

    fetch('http://localhost:3001/questions' + (page ? ('?page=' + page) : '') + (!page && id ? ('?id=' + id) : '') )
        .then((data) => data.json())
        .then((res) => this.setState({ questions: res.data }));
  };

  postDataToDB = (message) => {
    let currentIds = this.state.questions.map((data) => data.id);
    let idToBeAdded = 0;
    while (currentIds.includes(idToBeAdded)) {
      ++idToBeAdded;
    }

    axios.post('http://localhost:3001/questions', {
      id: idToBeAdded,
      message: message
    }).then((res) => this.getDataFromDb());
  };

  deleteFromDB = (idTodelete) => {
    parseInt(idTodelete);
    let objIdToDelete = null;
    this.state.questions.forEach((dat) => {
      if (dat.id == idTodelete) {
        objIdToDelete = dat._id;
      }
    });

    axios.delete('http://localhost:3001/questions', {
      data: {
        id: objIdToDelete,
      }
    });
  };

  updateDB = (idToUpdate, updateToApply) => {
    let objIdToUpdate = null;
    parseInt(idToUpdate);
    this.state.questions.forEach((dat) => {
      if (dat.id == idToUpdate) {
        objIdToUpdate = dat._id;
      }
    });

    axios.put('http://localhost:3001/questions', {
      id: objIdToUpdate,
      update: { message: updateToApply },
    });
  };

  render() {
    const { questions } = this.state;

    return (
      <div className="container">
        <Table questionsData={questions} getQuestion={this.getQuestion} removeQuestion={this.removeQuestion} />
        <Form handleSubmit={this.handleSubmit} />
      </div>
    )
  };
}

export default App;