import React, { Component } from 'react'

class Form extends Component {
  	initialState = {
    	name: '',
    	email: '',
    	obs: '',
    	date: ''
  	}

  	state = this.initialState

	submitForm = () => {
	  	this.props.handleSubmit(this.state)
	  	this.setState(this.initialState)
	}

  	handleChange = event => {
	  	const { name, value } = event.target

	  	this.setState({
	    	[name]: value,
	  	})
	}

	render() {
	  const { name, email, obs, date } = this.state;

	  return (
	    <form>
	      <label htmlFor="name">Name</label>
	      <input type="text" name="name" id="name" value={name} onChange={this.handleChange} />
	      <label htmlFor="email">E-mail</label>
	      <input type="email" name="email" id="email" value={email} onChange={this.handleChange} />
	      <label htmlFor="obs">Obs</label>
	      <textarea name="obs" id="obs" value={obs} onChange={this.handleChange}></textarea>
	      <label htmlFor="date">Date</label>
	      <input type="date" name="date" id="date" value={date} onChange={this.handleChange} />
	      <button type="button" onClick={this.submitForm}>Submit</button>
	    </form>
	  );
	}
}

export default Form