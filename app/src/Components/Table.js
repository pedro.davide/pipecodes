import React, { Component } from 'react'

const TableHeader = () => {
  return (
    <thead>
      <tr>
        <th>Name</th>
        <th>E-mail</th>
        <th>Obs</th>
        <th>Date</th>
        <th>ID</th>
        <th></th>
      </tr>
    </thead>
  )
}

const TableBody = props => {
  const rows = props.questionsData.map((row, index) => {
    return (
      <tr key={index}>
        <td>{row.name}</td>
        <td>{row.email}</td>
        <td>{row.obs}</td>
        <td>{row.date}</td>
        <td>{row._id}</td>
        <td>
          <button className="accent-button" onClick={() => props.getQuestion(row._id)}>Details</button>
          <button onClick={() => props.removeQuestion(index)}>Delete</button>
        </td>
      </tr>
    )
  });

  return <tbody>{rows}</tbody>
}

class Table extends Component {
  render() {
    const { questionsData,removeQuestion,getQuestion } = this.props;

    return (
      <table>
        <TableHeader />
        <TableBody questionsData={questionsData} removeQuestion={removeQuestion} getQuestion={getQuestion} />
      </table>
    );
  }
}

export default Table