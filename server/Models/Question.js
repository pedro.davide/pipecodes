const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const tomorrow = new Date();
tomorrow.setDate(tomorrow.getDate() + 1);

var validateEmail = function(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};

const QuestionSchema = new Schema({
    id: Number,
    name: {
    	type: String,
    	required: true
    },
    email: {
    	type: String,
        trim: true,
        lowercase: true,
        required: true,
        validate: [validateEmail, 'Please enter a valid e-mail address']
    },
    obs: String,
    date: {
    	type: Date,
    	min: tomorrow,
    	required: true
    }
},
{
	timestamps: true
});

module.exports = mongoose.model("Question", QuestionSchema);