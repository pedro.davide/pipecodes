const mongoose = require('mongoose');
const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const Question = require('./Models/Question');

const API_PORT = 3001;
const app = express();
app.use(cors());
const router = express.Router();

// this is our MongoDB database
const dbRoute ='mongodb://localhost/data';

// connects our back end code with the database
mongoose.connect(dbRoute, { useNewUrlParser: true });

let db = mongoose.connection;

db.once('open', () => console.log('connected to the database'));

// checks if connection with the database is successful
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// (optional) only made for logging and
// bodyParser, parses the request body to be a readable json format
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger('dev'));

// this is our get method
// this method fetches all available data in our database
router.get('/questions', (req, res) => {
  const { page = 1, limit = 20, id = null } = req.query;

  if (id) {
    Question.findById(id, (err, data) => {
      if (err) return res.json({ success: false, error: err });
      return res.json({ success: true, data: [data] });
    });
  }
  else {
    Question.find((err, data) => {
      if (err) return res.json({ success: false, error: err });
      return res.json({ success: true, data: data });
    }).limit(limit * 1).skip((page - 1) * limit);
  }
});

router.get('/questions/:id', (req, res) => {  
  console.log(req.params.id);

  Question.findById(req.params.id, (err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: [data] });
  });
});

// this is our update method
// this method overwrites existing data in our database
router.put('/questions', (req, res) => {
  const { id, update } = req.body;
  Question.findByIdAndUpdate(id, update, (err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

// this is our delete method
// this method removes existing data in our database
router.delete('/questions', (req, res) => {
  const { id } = req.body;
  Question.findByIdAndRemove(id, (err) => {
    if (err) return res.send(err);
    return res.json({ success: true });
  });
});

// this is our create methid
// this method adds new data in our database
router.post('/questions', (req, res) => {
  let question = new Question();

  const { id, message } = req.body;

  if ((!id && id !== 0) || !message) {
    return res.json({
      success: false,
      error: 'INVALID INPUTS',
    });
  }
  question.name = message.name;
  question.email = message.email;
  question.obs = message.obs;
  question.date = message.date;

  question.id = id;
  question.save((err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

// append /api for our http requests
app.use('/', router);

// launch our backend into a port
app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));